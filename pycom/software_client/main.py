from network import WLAN
import time
import socket
import os

# configure the WLAN subsystem in station mode (the default is AP)
wlan = WLAN(mode=WLAN.STA)
# go for fixed IP settings (IP, Subnet, Gateway, DNS)
wlan.ifconfig(config=('192.168.4.101', '255.255.255.0', '192.168.4.1', '192.168.4.1'))
wlan.scan()     # scan for available networks
wlan.connect(ssid='weather_station')

try:
    while True:
        if not wlan.isconnected():
            wlan.connect(ssid='weather_station')
        while not wlan.isconnected():
            print('.', end='')
            time.sleep(0.1)
        s = socket.socket()
        s.connect(('192.168.4.1', 5000))
        s.send(str(int((os.urandom(1)[0] / 256)*1000 + 800))) # 8-99.99 m/s
        data = s.recv(50)
        res = "Data sent... (" +  data.decode() + ")" 
        s.close()
        print(res)
        time.sleep(5)
except Exception as e:
    print(e)