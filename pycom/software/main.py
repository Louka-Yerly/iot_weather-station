from network import LoRa
import socket
import time
import ubinascii
import config
import json
from time import sleep
from pycoproc_1 import Pycoproc
from SI7006A20 import SI7006A20
from network import WLAN
import select



wlan = WLAN()

wlan.init(mode=WLAN.AP, ssid='weather_station')

print(wlan.ifconfig(id=1)) #id =1 signifies the AP interface

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('', 5000))
server_socket.listen(5)

windspeed = 10.3 # m/s
def handle_wifi_client(client):
    global windspeed
    print("New data from client")
    windspeed = int(client.recv(50).decode())/100.0
    client.send("ok")
    client.close()


# Initialise LoRa in LORAWAN mode.
# Please pick the region that matches where you are using the device:
# Asia = LoRa.AS923
# Australia = LoRa.AU915
# Europe = LoRa.EU868
# United States = LoRa.US915
lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868)

# create an OTAA authentication parameters, change them to the provided credentials
app_eui = ubinascii.unhexlify('0000000000000000')
app_key = ubinascii.unhexlify(config.APP_KEY)
#uncomment to use LoRaWAN application provided dev_eui
dev_eui = ubinascii.unhexlify(config.DEV_EUI)

# join a network using OTAA (Over the Air Activation)
#uncomment below to use LoRaWAN application provided dev_eui
#lora.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0)
lora.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0)

# wait until the module has joined the network
while not lora.has_joined():
    time.sleep(2.5)
    print('Not yet joined...')

print('Joined')
# create a LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# set the LoRaWAN data rate
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)

# make the socket blocking
# (waits for the data to be sent and for the 2 receive windows to expire)
s.setblocking(True)

py = Pycoproc(Pycoproc.PYSENSE)
si = SI7006A20(py)


def payload(humidity, temperature) -> str:
    global windspeed
    hum_int = int(humidity*100)
    temp_int = int(temperature*100)
    wind_int = int(windspeed*100)
    print("temp:", temperature, "°C,  hum:", humidity, "%,  wind_speed:", windspeed, "m/s")
    return temp_int.to_bytes(4, 'big') + hum_int.to_bytes(4, 'big') + wind_int.to_bytes(4, 'big') + bytes(config.GEO_PLACEMENT,'UTF-8')

def get_humidity() -> float:
    return si.humidity()

def get_temperature() -> float:
    return si.temperature()

while(True):
    sleep(5)
    r,w,err = select.select((server_socket,), (), (), 1)
    if r:
        for readable in r:
            try:
                client, client_addr = server_socket.accept()
                handle_wifi_client(client)
            except OSError as ose:
                print(ose)
    
    # send some data
    s.send(bytes(payload(get_humidity(), get_temperature())))
    print("Data sent")


# send some data
s.send(bytes("{test: 15.2}", 'utf-8'))

# make the socket non-blocking
# (because if there's no data received it will block forever...)
s.setblocking(False)

# get any data received (if any...)
data = s.recv(64)
print(data)