/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateThing = /* GraphQL */ `
  subscription OnCreateThing($filter: ModelSubscriptionThingFilterInput) {
    onCreateThing(filter: $filter) {
      id
      location
      temperature
      humidity
      data {
        timestamp
        name
        temperature
        humidity
        id
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateThing = /* GraphQL */ `
  subscription OnUpdateThing($filter: ModelSubscriptionThingFilterInput) {
    onUpdateThing(filter: $filter) {
      id
      location
      temperature
      humidity
      data {
        timestamp
        name
        temperature
        humidity
        id
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteThing = /* GraphQL */ `
  subscription OnDeleteThing($filter: ModelSubscriptionThingFilterInput) {
    onDeleteThing(filter: $filter) {
      id
      location
      temperature
      humidity
      data {
        timestamp
        name
        temperature
        humidity
        id
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateData = /* GraphQL */ `
  subscription OnCreateData($filter: ModelSubscriptionDataFilterInput) {
    onCreateData(filter: $filter) {
      timestamp
      name
      temperature
      humidity
      id
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateData = /* GraphQL */ `
  subscription OnUpdateData($filter: ModelSubscriptionDataFilterInput) {
    onUpdateData(filter: $filter) {
      timestamp
      name
      temperature
      humidity
      id
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteData = /* GraphQL */ `
  subscription OnDeleteData($filter: ModelSubscriptionDataFilterInput) {
    onDeleteData(filter: $filter) {
      timestamp
      name
      temperature
      humidity
      id
      createdAt
      updatedAt
    }
  }
`;
