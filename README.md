# IoT_weather-station

Description of this project:
- **aws:** folder that contains the script to automize the deployement (provisioning) of a new device
![provisioning](provisioning.png)
- **pycom:** folder that contains all the code for the pycom devices.
![wifi](provisioning-Page-2.png)
- **ws-app**: folder that contains the source code of the dashboard 