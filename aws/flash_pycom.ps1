# activate venv
..\..\activate_venv.ps1

# copy the files
cp config.py ..\pycom\software
echo "Generated files copied to '..\pycom\software'"

# move in the pycom folder
cd ..\pycom\software

# upload files
ampy -p $args[0] put .\main.py
echo "Finish copied /flash/main.py"
ampy -p $args[0] put .\config.py
echo "Finish copied /flash/config.py"
ampy -p $args[0] put lib
echo "Finish /flash/lib/*"

# reboot the board
ampy -p $args[0] reset

