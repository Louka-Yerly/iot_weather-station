# Credentials

Before using Boto3, you need to set up authentication credentials for your AWS account using either the IAM Console or the AWS CLI. You can either choose an existing user or create a new one.


# Run the script

```shell-session
# cd iot_weather-station\aws
# ..\..\activate_venv.ps1
# python provision2.py <geo_placement> <thing_name>