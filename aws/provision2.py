import os
import binascii
from boto3 import client as cli
from botocore.config import Config
import subprocess
import serial.tools.list_ports
import generate_config_pycom

# HARDCODED:
ACCESS_KEY = "AKIAQW7FXZOKA5X6O7PU"
SECRET_KEY = "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"
# HARDCODED:




class ThingProvision():

    THING_TYPE_LORAWAN = "lorawan"
    THING_STACK_NAME = "TTN-Integration"
    THING_LORAWAN_VERSION = "1.0.2"
    THING_REGIONAL_PARAMETERS_VERSION = "1.0.2"
    THING_JOIN_EUI = "0000000000000000"

    def __init__(self, aws_access_key_id, aws_secret_access_key) -> None:
        config = Config(
            region_name = 'eu-central-1',
            signature_version = 'v4',
            retries = {
                'max_attempts': 10,
                'mode': 'standard'
            }
        )
        
        self.__client = cli(
            'iot',
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            config=config
        )



    def provision(self, thing_name: str, app_key: str, dev_eui: str):
        self.__create_thing(thing_name, app_key, dev_eui)


    def __create_thing(self, thing_name: str, app_key, dev_eui: str):
        response =  self.__client.create_thing(
            thingName=thing_name,
            thingTypeName=ThingProvision.THING_TYPE_LORAWAN,
            attributePayload={
                'attributes': {
                    'devEUI': dev_eui,
                    'stackName': ThingProvision.THING_STACK_NAME,
                    'appKey': app_key,
                    'joinEUI': ThingProvision.THING_JOIN_EUI,
                    'lorawanVersion': ThingProvision.THING_LORAWAN_VERSION,
                    'regionalParametersVersion': ThingProvision.THING_REGIONAL_PARAMETERS_VERSION
                },
                "merge": False
            })


def get_dev_eui() -> str:
    com = serial.tools.list_ports.comports()[0].device
    print("Using port", com)
    result = subprocess.run(["..\\pycom\\pycom-fwtool-cli.exe", "-p", com, "smac"], capture_output=True, text=True)
    dev_eui = result.stdout.split("\n")[1].split("=")[1]
    print(f"DEV_EUI read on cheap: {dev_eui}")
    return dev_eui

def get_app_key():
    return binascii.b2a_hex(os.urandom(16)).decode('utf-8').upper()

def generate_config_file(geo_placement: str, dev_eui: str, app_key: str):
    generate_config_pycom.generate(geo_placement, dev_eui, app_key)
    com = serial.tools.list_ports.comports()[0].device
    subprocess.run(["powershell.exe", "D:\\Louka\\MSE\\IoT\\iot_weather-station\\aws\\flash_pycom.ps1", com])


def provision_lorawan(geo_placement, thing_name="",):
    dev_eui = get_dev_eui()
    app_key = get_app_key()
    if thing_name == "":
        print("No thing_name given... Using dev_eui as thing_name")
        thing_name = dev_eui
    
    print(f"dev_eui: {dev_eui}\napp_key: {app_key}")
    provision = ThingProvision(ACCESS_KEY, SECRET_KEY)
    provision.provision(thing_name, app_key, dev_eui)

    generate_config_file(geo_placement, dev_eui, app_key)


if __name__ == "__main__":
    import sys
    if len(sys.argv) < 2:
        print("Usage: \n"
              "\t python provision <geo_placement> [<thing_name>]\n"
              "=========\n"
              "<geo_placement>: the location of the weather station (insert dash (\"-\") string if not none)\n"
              "<thing_name>   : choose a thing_name. By default the dev_eui is used\n")

    else:

        geo_placement = sys.argv[1]
        thing_name = ""
        if len(sys.argv) > 2:
            thing_name = sys.argv[2]

        provision_lorawan(geo_placement, thing_name)
        print("=================\n"
            "Provision fnished\n"
            "=================")
    