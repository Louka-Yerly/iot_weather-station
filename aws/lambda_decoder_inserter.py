import json
import base64
import boto3
from decimal import Decimal
import time

def insert_in_db(temperature, humidity, wind, location, name):
    dynamodb = boto3.resource('dynamodb')
    #table name
    table = dynamodb.Table('Thing-cwqmfiwfzvelnao2aj65e5oppm-dev')
    #inserting values into table
    response = table.put_item(
       Item={
           'id': name,
            'temperature': Decimal(f'{temperature:.2f}'),
            'humidity': Decimal(f'{humidity:.2f}'),
            'wind': Decimal(f'{wind:.2f}'),
            'location': location,
        }
    )
    
    table = dynamodb.Table('Data-cwqmfiwfzvelnao2aj65e5oppm-dev')
    #inserting values into table
    response = table.put_item(
       Item={
           'id': str(int(time.time())),
            'temperature': Decimal(f'{temperature:.2f}'),
            'humidity': Decimal(f'{humidity:.2f}'),
            'wind': Decimal(f'{wind:.2f}'),
            'name': name
            'timestamp': str(int(time.time())),
        }
    )
    
    
    return response

def extract_data(event):
    dev_eui = event['end_device_ids']['dev_eui']
    payload = event['uplink_message']['frm_payload']
    payload_bytes = base64.b64decode(payload)
    
    temp = int.from_bytes(payload_bytes[0:4], 'big')
    hum = int.from_bytes(payload_bytes[4:8], 'big')
    wind = int.from_bytes(payload_bytes[8:12], 'big')
    location = payload_bytes[12:].decode('utf-8')
    print(temp, hum, wind, location)
    
    return dev_eui, temp/100, hum/100, wind/100, location


def send_notification(message):
    client = boto3.client('sns')
    response = client.publish (
        TargetArn = "arn:aws:sns:eu-central-1:049337060244:weather_alert",
        Message = message,
        MessageStructure = 'text'
   )

def check_data(temperature, humidity):
    message = "Warning: "
    need_send = False
    if temperature < 1.0:
        message = message + f"risk of frost (temperature: {temperature}), "
        need_send = True
    if humidity > 80.0:
        message = message + f"high humidity (humidity: {humidity}), "
        need_send = True
    
    if need_send:
        send_notification(message[:-2])
    
    

def lambda_handler(event, context):
    # TODO implement
    dev_eui, temperature, humidity, wind, location = extract_data(event)
    
    insert_in_db(temperature, humidity, wind, location, dev_eui)
    
    check_data(temperature, humidity)
    
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
