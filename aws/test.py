import base64

data = {'end_device_ids': {
            'device_id': 'eui-70b3d549941f660f', 
            'application_ids': {
                'application_id': 'ma-iot-weather-station1'
                },
            'dev_eui': '70B3D549941F660F',
            'join_eui': '0000000000000000',
            'dev_addr': '260BEE68'
            },
        'correlation_ids': [
            'as:up:01GMAMF09QY9XPWQ1E0KP8RC5D', 'gs:conn:01GMABTM8VJG8PYNS7EK0ENJFC', 'gs:up:host:01GMABTM8YFT8HM16BCQ6A52DX', 'gs:uplink:01GMAMF035CDJC71Y3V40K9CKP', 'ns:uplink:01GMAMF0366VCZ7YR7GSH1SQEZ', 'rpc:/ttn.lorawan.v3.GsNs/HandleUplink:01GMAMF036RG1JTPBVNC8HWRC4', 'rpc:/ttn.lorawan.v3.NsAs/HandleUplink:01GMAMF09PVQQYYWKP34SAJ9CD'],
        'received_at': '2022-12-15T10:14:45.815119215Z',
        'uplink_message': {
            'session_key_id': 'AYUVNXdbL0Pkr4EiEsMmWQ==',
            'f_port': 2,
            'f_cnt': 90,
            'frm_payload': 'AAAJ+QAAEgM=',
            'rx_metadata': [{'gateway_ids': {'gateway_id': 'eui-b827ebfffe612156', 'eui': 'B827EBFFFE612156'}, 'time': '2022-12-15T10:14:45.583994Z', 'timestamp': 468546179, 'rssi': -29, 'channel_rssi': -29, 'snr': 10, 'uplink_token': 'CiIKIAoUZXVpLWI4MjdlYmZmZmU2MTIxNTYSCLgn6//+YSFWEIPltd8BGgwIlefrnAYQ0d/ioAIguJ+dvNGHAg==', 'channel_index': 3, 'received_at': '2022-12-15T10:14:45.605597649Z'}],
            'settings': {
                'data_rate': {'lora': 
                    {'bandwidth': 125000, 'spreading_factor': 7, 'coding_rate': '4/5'}},
                    'frequency': '867100000',
                    'timestamp': 468546179, 'time': '2022-12-15T10:14:45.583994Z'
                },
            'received_at': '2022-12-15T10:14:45.606383974Z',
            'consumed_airtime': '0.056576s',
            'network_ids': {'net_id': '000013', 'tenant_id': 'ttn', 'cluster_id': 'eu1', 'cluster_address': 'eu1.cloud.thethings.network'}
            }
        }
dev_eui = data['end_device_ids']['dev_eui']
payload = data['uplink_message']['frm_payload']
payload_bytes = base64.b64decode(payload)

temp = int.from_bytes(payload_bytes[0:4], 'big')
hum = int.from_bytes(payload_bytes[4:8], 'big')
location = payload_bytes[8:].decode('utf-8')

print(dev_eui, temp/100.0, hum/100.0, location)