
def generate(geo_placement, dev_eui, app_key):
    config = f"""# LoRaWAN configuration
DEV_EUI = '{dev_eui}'
APP_KEY = '{app_key}'

# Device configuration
GEO_PLACEMENT = '{geo_placement}'
"""
    with open("config.py", 'wb') as op:
        op.write(config.encode())