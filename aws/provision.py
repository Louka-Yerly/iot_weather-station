from boto3 import client as cli
from botocore.config import Config
import json
import subprocess
import serial.tools.list_ports

# HARDCODED:
ACCESS_KEY = "AKIAQW7FXZOKA5X6O7PU"
SECRET_KEY = "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"
# HARDCODED:


class ThingProvision():
    AWS_CERT_CA = "aws_root_ca.pem"
    AWS_CERT_CRT = "aws_client.cert"
    AWS_CERT_PRIVATE = "aws_private.key"

    def __init__(self, aws_access_key_id, aws_secret_access_key, ) -> None:
        config = Config(
            region_name = 'eu-west-3',
            signature_version = 'v4',
            retries = {
                'max_attempts': 10,
                'mode': 'standard'
            }
        )
        
        self.__client = cli(
            'iot',
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            config=config
        )



    def provision(self, thing_name: str, group_name: str, policy_name: str):
        self.__create_thing(thing_name, group_name)
        return self.__create_certificate(thing_name, policy_name)


    def __create_thing(self, thing_name: str, group_name: str):
        response =  self.__client.create_thing(thingName=thing_name,)

        response = self.__client.add_thing_to_thing_group(
            thingGroupName=group_name,
            thingName=thing_name,
        )
        
    def __create_certificate(self, thing_name: str, policy_name: str):
        response = self.__client.create_keys_and_certificate(setAsActive=True)

        data = json.loads(json.dumps(response, sort_keys=False, indent=4))
        
        public_key = ""
        private_key = ""
        certificate_pem = ""
        certificate_arn = ""

        for element in data: 
                if element == 'certificateArn':
                        certificate_arn = data['certificateArn']
                elif element == 'keyPair':
                        public_key = data['keyPair']['PublicKey']
                        private_key = data['keyPair']['PrivateKey']
                elif element == 'certificatePem':
                        certificate_pem = data['certificatePem']
                                
        with open(ThingProvision.AWS_CERT_PRIVATE, 'w') as outfile:
                outfile.write(private_key)
        with open(ThingProvision.AWS_CERT_CRT, 'w') as outfile:
                outfile.write(certificate_pem)

        response = self.__client.attach_policy(
            policyName = policy_name,
            target = certificate_arn
        )
        response = self.__client.attach_thing_principal(
            thingName = thing_name,
            principal = certificate_arn
        )
        return private_key, certificate_pem

def provision(board, thing_name="boto3_device", group_name="Pycom", policy_name="iot_inst-ind_policy",
              ssid="_S8", password="12345678", endpoint="a63lpnh93z01r-ats.iot.eu-west-3.amazonaws.com", ):    
        
    prov = ThingProvision(ACCESS_KEY, SECRET_KEY)
    private_key, certificate_pem = prov.provision(thing_name=thing_name, group_name=group_name, policy_name=policy_name)

    if board == "esp32":
        import generate_secret_esp32
        generate_secret_esp32.generate(thing_name, ssid, password, endpoint, certificate_pem, private_key)

        com = serial.tools.list_ports.comports()[0].device
        print("Using port", com)
        # HARDCODED: change the hardcoded path
        subprocess.run(["powershell.exe", "D:\\Louka\\MSE\\IoT\\iot_inst-ind\\aws\\flash_esp32.ps1", com])    
    elif board == "pycom":
        import generate_config_pycom
        generate_config_pycom.generate(thing_name, ssid, password, endpoint)
        com = serial.tools.list_ports.comports()[0].device
        print("Using port", com)
        # HARDCODED: change the hardcoded path
        subprocess.run(["powershell.exe", "D:\\Louka\\MSE\\IoT\\iot_inst-ind\\aws\\flash_pycom.ps1", com])

    else:
        print("Flashing not yet implemented for this platform")
        return 1
    
    
    print("=================\n"
          "Provision fnished\n"
          "=================")
    return 0


def get_thing_name(board):
    if board == "esp32":
        com = serial.tools.list_ports.comports()[0].device
        result = subprocess.run(["esptool.py.exe", "-p", com, "read_mac"], capture_output=True, text=True)
        mac = result.stdout.split("\n")[-3].split(" ")[-1]
        return mac.lower()

    elif board == "pycom":
        com = serial.tools.list_ports.comports()[0].device
        result = subprocess.run(["..\\pycom\\pycom-fwtool-cli.exe", "-p", com, "wmac"], capture_output=True, text=True)
        wmac = result.stdout.split()[-1].split("=")[1]
        mac = ""
        for i in range(len(wmac)):
            if i%2 == 0:
                mac += ":"
            mac += wmac[i]

        return mac[1:].lower()


    else:
        print("Automatic id not yet implemented for this platform")
        return "default"


if __name__ == "__main__":
    import sys

    args = {"board": "esp32", "group_name": "Pycom", "policy_name": "iot_inst-ind_policy", "thing_name": "boto3_device"}
    if len(sys.argv) < 2:
        print("Usage: \n"
              "\t python provision <board> <thing_name> <group_name> <policy_name>\n"
              "=========\n"
              "\t <board>: esp32 or pycom\n"
              "\t <group_name>: group to use in AWS (default is <board>)\n"
              "\t <policy_name>: AWS policy")
        
        exit(1)

    else:
            
        args["group_name"] = sys.argv[1]
        names = list(args.keys())
        for i in range(1, len(sys.argv)):
            args[names[i-1]] = sys.argv[i]
        
        args["thing_name"] = get_thing_name(args["board"])

        
        print("Provision with:", args)
        ret = provision(args["board"], args["thing_name"], args["group_name"], args["policy_name"])

        exit(ret)

